<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a view
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array = [
            "foo" => "bar",
            "bar" => "foo"
        ];
        \Debugbar::info($array);
        \Debugbar::error('Error!');
        \Debugbar::warning('Watch out…');
        \Debugbar::addMessage('Another message', 'mylabel');
        return view('user.list',['user' => 'Bot']);
    }
}
