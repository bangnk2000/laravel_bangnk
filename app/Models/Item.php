<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $table = 'items';

    /**
     * The belongs to Relationship
     *
     * @return App/Models/Category
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
